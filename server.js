"use strict";

// dépend de phantomjs 1.8
// testé avec phantomjs 1.9.1 sur ubuntu et debian wheezy
// lancer avec: phantomjs server.js

// todo: voir https://gitorious.org/phan/pages/Todo

function errorImage(response) {
    response.statusCode = 301; // permanent redirect to "error" image
    response.setHeader('Location', 'http://i.stack.imgur.com/efM6b.jpg');       
    response.closeGracefully();
}

var server = require('webserver').create(),
    service = server.listen(8080, { 'keepAlive': true }, function(request, response) {
    // parse un URL de la forme suivante:
    // [/WidthxHeight][/ZoomFactor][/jpeg]/http://example.com/path?query=bof
    // Les arguments dans [] sont optionnels
    // L'ordre est important
    // Par défaut:  width = 1024; height = 768
    //      zoom = 1 (.2, 0.125, 1.5, etc.)
    //      type = png (jpeg étant la seule autre option possible)
    // L'URL doit absolument commencer par http (ou https)
    //
    // NOTE: Le ZoomFactor multiplie les dimensions.
    // Par exemple, si on donne 1000x800 et 1.5 comme zoom,
    // l'image générée aura une taille de 1500x1200.
    // Si on donne 1000x800 et 0.5 (ou .5) comme zoom,
    // l'image générée aura une taille de 500x400.
    // Nous utilisons le regex pour imposer des limites à la taille et au zoom
    var type, size, page = require('webpage').create(),
        re = /^(\/[1-9]\d{0,3}x[1-9]\d{0,3})?(\/\d{0,1}\.?\d{0,3})?(\/jpeg)?\/(http.*)$/,       
        parts = request.url.match(re);

    // Nous n'arrivons pas à parser tous les arguments
    if (!(parts && (5 === parts.length) && parts[4])) {
        errorImage(response);
        return;
    }

    // Nous avons besoin du zoom pour multiplier les dimensions s'il y a lieu
    page.zoomFactor = parts[2] ? parts[2].slice(1) : 1;

    // Nous cherchons le format WidthxHeight où "x" est la lettre
    // et width et height sont des entiers
    size = parts[1] ? parts[1].match(/\/([1-9]\d{0,3})x([1-9]\d{0,3})/) : [null, 1024, 768];
    page.clipRect = page.viewportSize = {
        width: size[1] * page.zoomFactor,
        height: size[2] * page.zoomFactor
    };

    // Par défaut, nous utilisons le format d'image PNG
    type = parts[3] ? parts[3].slice(1) : 'png';

    // parts[4] contient une version encodée de l'URL
    page.open(decodeURIComponent(parts[4]), function () {
        // page.render() sauve l'image sur le disque
        // tandis que page.renderBase64() retourne l'image
        // comme une chaine qu'il faut convertir avec atob
        var out = atob(page.renderBase64(type));

        // une erreur est survenue
        if ('' === out) {
            errorImage(response);
            return;
        }

        response.statusCode = 200;
        response.setHeader('Content-Type', 'image/' + type);
        response.setHeader('Content-Length', out.length);
        response.setEncoding("binary");
        response.write(out);
        response.close();
    });
});
